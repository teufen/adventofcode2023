orig = [line.strip('\n') for line in open('input11.txt', 'r')]
#orig = [line.strip('\n') for line in open('example11.txt', 'r')]

galaxies = set()
extra_rows = 0
for row, line in enumerate(orig):
    for column, c in enumerate(line):
        if c == '#':
            galaxies.add(complex(row + extra_rows, column))
    # add extra row for each empty row
    if line.count('#') == 0:
        extra_rows += 999999

# add extra column for each empty column:
extra_columns = 0
for column in range(len(orig[0])):
    if [orig[i][column] for i in range(len(orig))].count('#') == 0:
        for galaxy in [g for g in galaxies if g.imag > column + extra_columns]:
            galaxies.remove(galaxy)
            galaxies.add(galaxy + complex(0,999999))
        extra_columns += 999999

total_distance = 0
for galaxy in galaxies:
    for other in galaxies - {galaxy}:
        dist = other - galaxy
        dist = abs(dist.real) + abs(dist.imag)
        total_distance += dist

print('puzzle one: {:d}'.format(int(total_distance/2)))
#print('puzzle two: {:d}'.format())

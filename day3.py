orig = [line.strip('\n') for line in open('input3.txt', 'r')]
#orig = [line.strip('\n') for line in open('example3.txt', 'r')]

# for each line:
# - parse numbers with value and end coordinates
# - parse symbols with coordinates

symbols = []
selected_numbers = []
unchecked_numbers = []
number = ''
gear_symbols = {}

def symbol_around_number(number, end):
    # number = int, end = a+bj
    start = complex(end.real, end.imag - len(number) + 1)
    for symbol in symbols:
        if symbol.real in range(max(0, int(start.real) - 1), min(len(orig), int(end.real) + 2)) \
                and symbol.imag in range(max(0, int(start.imag) - 1), min(len(orig[0]), int(end.imag) + 2)):
                    if symbol in gear_symbols.keys():
                        gear_symbols[symbol].append(int(number))
                    return True
    return False


for row, line in enumerate(orig):
    for column,char in enumerate(line):
        if char.isdigit():
            number += char
            end_coordinates = complex(row, column)
        else:
            if char != '.':
                # symbol
                symbols.append(complex(row, column))
                if char == '*':
                    gear_symbols[complex(row, column)] = []
            if number != '':
                # end of number
                unchecked_numbers.append([number, end_coordinates])
                number = ''
    if number != '':
        if symbol_around_number(number, end_coordinates):
            selected_numbers.append(int(number))
        else:
            unchecked_numbers.append([number, end_coordinates])
        number = ''

for [number, end_coordinates] in unchecked_numbers:
    if symbol_around_number(number, end_coordinates):
        selected_numbers.append(int(number))
print('puzzle one: the sum is {:d}'.format(sum(selected_numbers)))

gear_ratios = []
for symbol in gear_symbols.keys():
    if len(gear_symbols[symbol]) == 2:
        gear_ratios.append(gear_symbols[symbol][0] * gear_symbols[symbol][1])
print('puzzle two: the sum is {:d}'.format(sum(gear_ratios)))

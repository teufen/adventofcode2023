orig = [line.strip('\n') for line in open('input15.txt', 'r')]
#orig = [line.strip('\n') for line in open('example15.txt', 'r')]

total = 0
for step in orig[0].split(','):
    res = 0
    for char in step:
        res += ord(char)
        res = (res * 17) % 256
    total += res

print('puzzle one: {:d}'.format(total))
#print('puzzle two: {:d}'.format())

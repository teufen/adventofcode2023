orig = [line.strip('\n') for line in open('input10.txt', 'r')]
#orig = [line.strip('\n') for line in open('example10.txt', 'r')]
#orig = [line.strip('\n') for line in open('example102.txt', 'r')]
#orig = [line.strip('\n') for line in open('example103.txt', 'r')]
#orig = [line.strip('\n') for line in open('example104.txt', 'r')]

# pipes {symbol: {next: [connecting symbols],},}
pipes = {
        '|': {complex(-1, 0): ['|', '7', 'F'], complex(1, 0): ['|', 'L', 'J']},
        '-': {complex(0, -1): ['-', 'F', 'L'], complex(0, 1): ['-','7', 'J']},
        'L': {complex(-1, 0): ['|', '7', 'F'], complex(0, 1): ['-','7', 'J']},
        'J': {complex(-1, 0): ['|', '7', 'F'], complex(0, -1): ['-', 'F', 'L']},
        '7': {complex(1, 0): ['|', 'L', 'J'], complex(0, -1): ['-', 'F', 'L']},
        'F': {complex(1, 0): ['|', 'L', 'J'], complex(0, 1): ['-','7', 'J']},
        'S': {complex(-1, 0): ['|', '7', 'F'], 
                complex(0, -1): ['-', 'F', 'L'],
                complex(1, 0): ['|', 'L', 'J'], 
                complex(0, 1): ['-','7', 'J']}
        }

# extention {symbol: 3x3 matrix as list of strings}
extension = {
        '|': ['.|.', '.|.', '.|.'],
        '-': ['...','---','...'],
        'L': ['.|.','.L-','...'],
        'J': ['.|.','-J.','...'],
        '7': ['...','-7.','.|.'],
        'F': ['...','.F-','.|.'],
        'S': ['.|.','-S-','.|.'],
        '.': ['...','...','...'],
        }
mark_extension = {
        '|': [complex(0, -1), complex(0, 1)],
        '-': [complex(-1, 0), complex(1, 0)],
        'L': [complex(0, -1), complex(1, 0), complex(1, -1)],
        'J': [complex(0, 1), complex(1, 0), complex(1, 1)],
        '7': [complex(0, 1), complex(-1, 0), complex(-1, 1)],
        'F': [complex(0, -1), complex(-1, -1), complex(-1, 0)],
        }

pipe_map = []
pipe_map2 = []
for row, line in enumerate(orig):
    r = []
    r20 = []
    r21 = []
    r22 = []
    for c in line:
        r.append(c)
        r20.extend(extension[c][0])
        r21.extend(extension[c][1])
        r22.extend(extension[c][2])
    pipe_map.append(r)
    pipe_map2.append(r20)
    pipe_map2.append(r21)
    pipe_map2.append(r22)
    if 'S' in line:
        start = complex(row, line.index('S'))
        start2 = complex(row * 3 + 1, line.index('S') * 3 + 1)

def print_map(mp):
    for row in mp:
        print(''.join([str(x) for x in row]))

def find_next(pos, cntr):
    nxts = set()
    for step in pipes[pipe_map[int(pos.real)][int(pos.imag)]].keys():
        nxt = pos + step
        if nxt.real in range(len(pipe_map)) and nxt.imag in range(len(pipe_map[0])):
            symbol = pipe_map[int(nxt.real)][int(nxt.imag)]
            if symbol not in pipes.keys():
                continue
            elif symbol in pipes[pipe_map[int(pos.real)][int(pos.imag)]][step]:
                nxts.add(nxt)
    pipe_map[int(pos.real)][int(pos.imag)] = 'M'
    return nxts

loop_counter = -1
nxts = {start,}
while len(nxts) > 1 or nxts == {start,}:
    loop_counter += 1
    nexts = set()
    for nxt in nxts:
        result = find_next(nxt, loop_counter)
        for r in result:
            nexts.add(r)
    nxts = {x for x in nexts}
print_map(pipe_map)
print('puzzle one: {:d}'.format(loop_counter + 1))

# part 2
def find_next2(pos):
    nxts = set()
    for step in pipes[pipe_map2[int(pos.real)][int(pos.imag)]].keys():
        nxt = pos + step
        if nxt.real in range(len(pipe_map2)) and nxt.imag in range(len(pipe_map2[0])):
            symbol = pipe_map2[int(nxt.real)][int(nxt.imag)]
            if symbol not in pipes.keys():
                continue
            elif symbol in pipes[pipe_map2[int(pos.real)][int(pos.imag)]][step]:
                nxts.add(nxt)
    symbol = pipe_map2[int(pos.real)][int(pos.imag)]
    pipe_map2[int(pos.real)][int(pos.imag)] = 'M'
    #if symbol in mark_extension.keys():
        #for extra in mark_extension[symbol]:
            #nwpos = pos + extra
            #pipe_map2[int(nwpos.real)][int(nwpos.imag)] = 'M'
    return nxts

def mark(pos):
    x = int(pos.real)
    y = int(pos.imag)
    if pipe_map2[x][y] == 'M':
        return
    if x in (0, len(pipe_map2) - 1) or y in (0, len(pipe_map2[0]) - 1):
        pipe_map2[x][y] = 'O'
        return
    for p in [pos + complex(a, b) for (a, b) in [(-1, 0), (1, 0), (0, -1), (0, 1),#]]:
        (-1, -1), (-1, 1), (1, -1), (1, 1)]]:
        if pipe_map2[int(p.real)][int(p.imag)] == 'O':
            pipe_map2[x][y] = 'O'
            return
    pipe_map2[x][y] = 'I'
    return

nxts = {start2,}
while len(nxts) > 1 or nxts == {start2,}:
    nexts = set()
    for nxt in nxts:
        result = find_next2(nxt)
        for r in result:
            nexts.add(r)
    nxts = {x for x in nexts}
for pos in nxts:
    pipe_map2[int(pos.real)][int(pos.imag)] = 'M'
print_map(pipe_map2)

def mark_all():
    for x in range((len(pipe_map2) // 2) + 1):
        for y in range((len(pipe_map2[0]) // 2) + 1):
            for pos in [complex(x, y), 
                    complex(x, len(pipe_map2[0]) - y -1),
                    complex(len(pipe_map2) - x - 1, y),
                    complex(len(pipe_map2) - x - 1, len(pipe_map2[0]) - y - 1)]:
                mark(pos)
    return

def count_i():
    cnt = 0
    for row in pipe_map2:
        for e in row:
            if e == 'I':
                cnt += 1
    return cnt

count = 0
prev_count = -1
while count != prev_count:
    prev_count = count
    mark_all()
    count = count_i()
for x in range(len(pipe_map2)):
    for y in range(len(pipe_map2[0])):
        if pipe_map2[x][y] == 'M':
            for i in [-1, 0, 1]:
                for j in [-1, 0, 1]:
                    if x+i in range(len(pipe_map2)) and y+j in range(len(pipe_map2[0])):
                        if pipe_map2[x+i][y+j] == 'I':
                            pipe_map2[x+i][y+j] = 'X' 

count = count_i()
print_map(pipe_map2)
print('puzzle two: {:d}'.format(count // 9))

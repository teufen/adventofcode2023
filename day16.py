orig = [line.strip('\n') for line in open('input16.txt', 'r')]
#orig = [line.strip('\n') for line in open('example16.txt', 'r')]

UP = complex(-1, 0)
DOWN = complex(1, 0)
LEFT = complex(0, -1)
RIGHT = complex(0, 1)

contraption = []
for line in orig:
    contraption.append(list(line))

def check(pos):
    if pos == None:
        return False
    if 0 <= pos['position'].real < len(contraption)\
            and 0 <= pos['position'].imag < len(contraption[0]):
        return True
    return False

def mirror(pos, symbol):
    if (symbol == '\\' and pos['direction'] == RIGHT)\
            or (symbol == '/' and pos['direction'] == LEFT):
        return {'position': pos['position'] + DOWN,
                'direction': DOWN}
    elif (symbol == '/' and pos['direction'] == RIGHT)\
            or (symbol == '\\' and pos['direction'] == LEFT):
        return {'position': pos['position'] + UP,
                'direction': UP}
    elif (symbol == '/' and pos['direction'] == DOWN)\
            or (symbol == '\\' and pos['direction'] == UP):
        return {'position': pos['position'] + LEFT,
                'direction': LEFT}
    elif (symbol == '\\' and pos['direction'] == DOWN)\
            or (symbol == '/' and pos['direction'] == UP):
        return {'position': pos['position'] + RIGHT,
                'direction': RIGHT}
    return None

def splitter(pos, symbol):
    if symbol == '-' and\
            (pos['direction'] == DOWN or pos['direction'] == UP):
        return ({'position': pos['position'] + LEFT, 'direction': LEFT},
                {'position': pos['position'] + RIGHT, 'direction': RIGHT})
    elif symbol == '|' and\
            (pos['direction'] == RIGHT or pos['direction'] == LEFT):
        return ({'position': pos['position'] + UP, 'direction': UP},
                {'position': pos['position'] + DOWN, 'direction': DOWN})
    return None, None

def advance(nxts):
    nw = []
    for pos in nxts:
        results.add(pos['position'])
        x = int(pos['position'].real)
        y = int(pos['position'].imag)
        if contraption[x][y] == '.'\
                or (pos['direction'].real == 0 and contraption[x][y] == '-')\
                or (pos['direction'].imag == 0 and contraption[x][y] == '|'):
            # empty space or pointy end of splitter
            nxt = {'position': pos['position'] + pos['direction'],
                'direction': pos['direction']}
            if check(nxt):
                nw.append(nxt)
        elif contraption[x][y] in ('\\', '/',):
            # mirror
            nxt = mirror(pos, contraption[x][y])
            if check(nxt):
                nw.append(nxt)
        else:
            # splitter
            nxt1, nxt2 = splitter(pos, contraption[x][y])
            if check(nxt1):
                nw.append(nxt1)
            if check(nxt2):
                nw.append(nxt2)
    return nw

results = set()
nexts = [{'position': complex(0,0), 'direction': RIGHT}]
while len(nexts) > 0:
    print(len(results))
    for pos in nexts:
        results.add(pos['position'])
    nexts = advance(nexts)

for x, row in enumerate(contraption):
    line = ''
    for y, pos in enumerate(row):
        if complex(x, y) in results:
            line += '#'
        else:
            #line += '.'
            line += contraption[x][y]
    print(line)
print('puzzle one: {:d}'.format(len(results)))
#print('puzzle two: {:d}'.format())

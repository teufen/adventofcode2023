orig = [line.strip('\n') for line in open('input9.txt', 'r')]
#orig = [line.strip('\n') for line in open('example9.txt', 'r')]

def get_next(ls, part2):
    if len(ls) > 1:
        nxt = [ls[i+1] - ls[i] for i in range(len(ls) - 1)]
        if all(y==0 for y in nxt):
            return 0
        return (nxt[0] if part2 else nxt[-1]) + (-1 if part2 else 1) * get_next(nxt, part2)
    print('Unable to process diffs of {0}'.format(ls))
    exit()

result1 = 0
result2 = 0
for line in orig:
    numbers = [int(x) for x in line.split(' ') if x]
    result1 += numbers[-1] + get_next(numbers, False)
    result2 += numbers[0] - get_next(numbers, True)

print('puzzle one: {:d}'.format(result1))
print('puzzle two: {:d}'.format(result2))

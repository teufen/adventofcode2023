orig = [line.strip('\n') for line in open('input8.txt', 'r')]
#orig = [line.strip('\n') for line in open('example8.txt', 'r')]
#orig = [line.strip('\n') for line in open('example82.txt', 'r')]

search = [int(x.replace('L', '0').replace('R', '1')) for x in orig[0]]
dests = {}
starts = []
ends = []
for line in orig[2:]:
    src, dst = [x.strip() for x in line.split(' = ')]
    dests[src] = dst.lstrip('(').rstrip(')').split(', ')
    if src[2] == 'A':
        starts.append(src)
     
start = 'AAA'
end = 'ZZZ'
destination = start
steps = 0
while destination != end:
    destination = dests[destination][search[steps%len(search)]]
    steps += 1

print('puzzle one: we needed {:d} steps'.format(steps))

def get_steps(start):
    steps = 0
    destination = start
    while destination[2] != 'Z':
        destination = dests[destination][search[steps%len(search)]]
        steps += 1
    return steps

steps = []
for start in starts:
    steps.append(get_steps(start))

def get_gcd(a, b):
    if b == 0:
        return a
    else:
        return get_gcd(b, a%b)

def get_lcm(ls):
    res = 1
    for x in ls:
        res = (res * x) // get_gcd(res, x)
    return res

print('puzzle two: we needed {:d} steps'.format(get_lcm(steps)))

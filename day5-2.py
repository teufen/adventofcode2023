orig = [line.strip('\n') for line in open('input5.txt', 'r')]
#orig = [line.strip('\n') for line in open('example5.txt', 'r')]

seeds = [int(x) for x in orig[0].split(':')[1].split(' ') if x]
seed_ranges = [(seeds[i], seeds[i] + seeds[i+1]) for i in range(0, len(seeds), 2)]
maps = []
mapp = []
for line in orig[1:]:
    if line.endswith('map:'):
        # new map start
        if mapp != []:
            maps.append(mapp)
        mapp = []
    elif line == '':
        continue
    else:
        mapp.append(tuple(int(x) for x in line.split(' ') if x))
maps.append(mapp)

def maprange(m, start, end):
	for dst, src, n in m:
		if src < end and start < src + n:
			ra = max(src, start)
			rb = min(src+n, end)
			yield (ra - src + dst, rb - src + dst)
			if start < src:
				yield from maprange(m, start, src)
			if end > src + n:
				yield from maprange(m, src+n, end)
			return
	yield (start, end)
	return

def mapranges(mp, seeds):
    nxt = []
    for rng in seeds:
        nxt.extend(maprange(mp, *rng))
    return nxt

for step in maps:
    seed_ranges = mapranges(step, seed_ranges)
    seed_ranges.sort()
print('puzzle two: lowest location = {:d}'.format(seed_ranges[0][0]))

orig = [line.strip('\n') for line in open('input2.txt', 'r')]
#orig = [line.strip('\n') for line in open('example2.txt', 'r')]

games1 = []
games2 = []
colors = ['red', 'green', 'blue']

def within_test(game):
    test_game = {'id': 0, 'red': 12, 'green': 13, 'blue': 14}
    for color in colors:
        if game[color] > test_game[color]:
            return False
    return True

for line in orig:
    g, c = [x.strip() for x in line.split(':')]
    game = {'id': int(g.split(' ')[1]), 'red': 0, 'green': 0, 'blue': 0}
    c = [x.split(',') for x in c.split(';')]
    for grab in c:
        for col in [x.strip() for x in grab]:
            [amount, color] = [el.strip() for el in col.split(' ')]
            if game[color] < int(amount):
                game[color] = int(amount)
    if within_test(game):
        games1.append(game)
    games2.append(game)
print('puzzle 1: the sum is {:d}'.format(sum([g['id'] for g in games1])))

def mult_ls(lst):
    r = 1
    for i in lst:
        r *= i
    return r

print('puzzle2: the sum of powers is {:d}'.format(
    sum([mult_ls([x[color] for color in colors]) for x in games2])))

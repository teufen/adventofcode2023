orig = [line.strip('\n') for line in open('input7.txt', 'r')]
#orig = [line.strip('\n') for line in open('example7.txt', 'r')]

types = ('poker', 'care', 'full', 'tris', 'dubbel', 'paar', 'kaart')

def get_hand_type1(hand):
    d = {}
    for card in hand:
        if card not in d.keys():
            d[card] = 1
        else:
            d[card] += 1
    a = sorted([x[1] for x in d.items()], reverse=True)
    if a[0] == 5:
        return types[0]
    elif a[0] == 4:
        return types[1]
    elif a[0] == 3:
        if a[1] == 2:
            return types[2]
        return types[3]
    elif a[0] == 2:
        if a[1] == 2:
            return types[4]
        return types[5]
    return types[6]

def get_hand_type(hand):
    d = {}
    jokers = 0
    for card in hand:
        if card == 'J':
            jokers += 1
            continue
        if card not in d.keys():
            d[card] = 1
        else:
            d[card] += 1
    a = sorted([x[1] for x in d.items()], reverse=True)
    if a == []:
        return types[0]
    if (a[0] + jokers) == 5:
        return types[0]
    elif a[0] + jokers == 4:
        return types[1]
    elif a[0] + jokers == 3:
        if a[1] == 2:
            return types[2]
        return types[3]
    elif a[0] + jokers  == 2:
        if a[1] == 2:
            return types[4]
        return types[5]
    return types[6]

def translate_hand(hand):
    cards = ('A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J')
    lttrs = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm')
    new_hand = ''
    for card in hand:
        new_hand += lttrs[cards.index(card)]
    return new_hand

hands = {}
for index, line in enumerate(orig):
    hand, bid = line.split(' ')
    new_hand = translate_hand(hand)
    hands[new_hand] = {'bid': int(bid), 'rank': 0, 'type': get_hand_type(hand)}

current_rank = len(orig)
for hand_type in types:
    sub_hands = sorted([x for x in hands.keys() if hands[x]['type'] == hand_type])
    for hand in sub_hands:
        hands[hand]['rank'] = current_rank
        current_rank -= 1

winnings = sum([hands[k]['bid'] * hands[k]['rank'] for k in hands.keys()])
print('puzzle one: total winnings are {:d}'.format(winnings))
#print('puzzle two: {:d}'.format())

#orig = [line.strip('\n') for line in open('input12.txt', 'r')]
orig = [line.strip('\n') for line in open('example12.txt', 'r')]

total = 0
for line in orig:
    pattern, numbers = line.split(' ')
    pattern = list(pattern)
    numbers = numbers.split(',')
    # find arrangements
    arrs = get_arrangements(pattern, number)
    total += arrs

print('puzzle one: {:d}'.format(total))
#print('puzzle two: {:d}'.format())

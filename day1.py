orig = [line.strip('\n') for line in open('input1.txt', 'r')]

# puzzle one
all_digits = []
for line in orig:
    b = []
    for e in line:
        try:
            b.append(int(e))
        except:
            continue
    b = int(str(b[0]) + str(b[-1]))
    all_digits.append(b)
print('puzzle one: the sum is {:d}'.format(sum(all_digits)))

# puzzle two
values = {'0': 0,
        '1': 1,
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        'one': 1,
        'two': 2,
        'three': 3,
        'four': 4,
        'five': 5,
        'six': 6,
        'seven': 7,
        'eight': 8,
        'nine': 9,
        }

def find_from_pos(pos, word):
    for i in range(1, 6):
        sli = word[pos:pos+i]
        if sli in values.keys():
            return values[sli]

def find_in_word(word):
    results = []
    for i in range(len(word)):
        found = find_from_pos(i, word)
        if found != None:
            results.append(found)
    return int(str(results[0]) + str(results[-1]))

all_digits = []
for word in orig:
    all_digits.append(find_in_word(word))
print('puzzle two: the sum is {:d}'.format(sum(all_digits)))

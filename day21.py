orig = [line.strip('\n') for line in open('input21.txt', 'r')]
STEPS = 26501365
#orig = [line.strip('\n') for line in open('example21.txt', 'r')]
#STEPS = 6

garden = [[i for i in line] for line in orig]
for x, row in enumerate(garden):
    if 'S' in row:
        start = complex(x, row.index('S'))
        garden[x][row.index('S')] = '.'

def move_one(pos, increment):
    nw = pos + increment
    if 0 <= nw.real < len(garden)\
            and 0 <= nw.imag < len(garden[0])\
            and garden[int(nw.real)][int(nw.imag)] == '.':
        return nw
    return None

def take_one_step(positions):
    new_positions = set()
    for pos in positions:
        for direction in [complex(-1, 0), complex(1, 0), complex(0, -1), complex(0, 1)]:
            nw = move_one(pos, direction)
            if nw:
                new_positions.add(nw)
    return new_positions

steps = 0
positions = set()
positions.add(start)
while steps < STEPS:
    positions = take_one_step(positions)
    steps += 1

print('puzzle one: {:d}'.format(len(positions)))
#print('puzzle two: {:d}'.format())

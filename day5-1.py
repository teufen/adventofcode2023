#orig = [line.strip('\n') for line in open('input5.txt', 'r')]
orig = [line.strip('\n') for line in open('example5.txt', 'r')]

seeds = [int(x) for x in orig[0].split(':')[1].split(' ') if x]
maps = []
mapp = []
for line in orig[1:]:
    if line.endswith('map:'):
        # new map start
        if mapp != []:
            maps.append(mapp)
        mapp = []
    elif line == '':
        continue
    else:
        destination, source, length = [int(x) for x in line.split(' ') if x]

        mapp.append({
            'source': range(source, source + length),
            'destination': range(destination, destination + length)
            })
maps.append(mapp)

def get_location(seed):
    current = seed
    for step in maps:
        for entry in step:
            if current in entry['source']:
                current = entry['destination'][entry['source'].index(current)]
                break
            else:
                continue
    return current

locations = []
for seed in seeds:
    locations.append(get_location(seed))

print('puzzle one: lowest location = {:d}'.format(min(locations)))

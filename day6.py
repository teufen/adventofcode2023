orig = [line.strip('\n') for line in open('input6.txt', 'r')]
#orig = [line.strip('\n') for line in open('example6.txt', 'r')]

times = [int(x) for x in orig[0].split(':')[1].split(' ') if x]
distances = [int(x) for x in orig[1].split(':')[1].split(' ') if x]

possibilities = []
for race in range(len(times)):
    speeds = []
    min_speed = int(distances[race] / times[race])
    for time in range(min_speed, times[race]):
        if time * (times[race] - time) > distances[race]:
            speeds.append(time)
    possibilities.append(len(speeds))

def mult_ls(lst):
    r = 1
    for i in lst:
        r *= i
    return r

print('puzzle one: {:d}'.format(mult_ls(possibilities)))
time = int(''.join([str(x) for x in times]))
distance = int(''.join([str(x) for x in distances]))
speeds = []
min_speed = int(distance / time)
for t in range(min_speed, time):
    if t * (time - t) > distance:
        speeds.append(t)
print('puzzle two: {:d}'.format(len(speeds)))

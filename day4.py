orig = [line.strip('\n') for line in open('input4.txt', 'r')]
#orig = [line.strip('\n') for line in open('example4.txt', 'r')]

def get_numbers(text):
    return [int(x) for x in text.split(' ') if x.isdigit()]

total1 = 0
occurances = [1 for _ in orig]
for index, card in enumerate(orig):
    card = card.split(':')[1]
    score = -1
    [winners, numbers] = card.split('|')
    winners = get_numbers(winners)
    for number in get_numbers(numbers):
        if number in winners:
            score += 1
    if score >= 0:
        total1 += 2**score
    score += 1 #part 2 score per card
    for i in range(index + 1, index + score + 1):
        occurances[i] += occurances[index]
print('Puzzle one: the total score is {:d}'.format(total1))
print('Puzzle two: the total score is {:d}'.format(sum(occurances)))

orig = [line.strip('\n') for line in open('input14.txt', 'r')]
#orig = [line.strip('\n') for line in open('example14.txt', 'r')]

def print_platform(pl):
    for row in pl:
        print(''.join(row))
    return

empty = '.'
rock = 'O'

def move_rock_north(pos):
    if pos.real == 0:
        return
    platform[int(pos.real)][int(pos.imag)] = empty
    pos -= complex(1, 0)
    platform[int(pos.real)][int(pos.imag)] = rock
    if platform[int(pos.real) - 1][int(pos.imag)] == empty:
        move_rock_north(pos)
    return

platform = []
for x, line in enumerate(orig):
    row = [x for x in line]
    platform.append(row)
    for y, loc in enumerate(row):
        if loc == rock:
            pos = complex(x, y)
            if platform[int(pos.real) - 1][int(pos.imag)] == empty:
                move_rock_north(pos)

print_platform(platform)
load = 0
for x, row in enumerate(platform):
    load += row.count('O') * (len(platform) - x)

print('puzzle one: {:d}'.format(load))
#print('puzzle two: {:d}'.format())
